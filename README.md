# Pre-Commit Semantic-Release


## Introduction
I'm preparing a very nice bugfix/feature my commits messages are not enforced, but I do want to use semantic-release for after the actual merge to the main branch. I need something to prevent me to 'miss' one of the commit message to signal to semantic-release what kind of release I'm building  (`feat:`, `bugfix:`, `doc:`, `chores:`, etc)

From :

```
$ git log origin..  --pretty=format:"%s"
```