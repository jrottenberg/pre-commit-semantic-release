# the inclusion of the tests module is not meant to offer best practices for
# testing in general, but rather to support the `find_packages` example in
# setup.py that excludes installing the "tests" package

import unittest

from semrel import check_line


class TestSimple(unittest.TestCase):

    def test_positive(self):
        line = "feat: add a great feature"
        self.assertEqual(check_line(line), True)

    def test_positive_with_context(self):
        line = "docs(README): test with context"
        self.assertEqual(check_line(line), True)

    def test_negative(self):
        line = "add a great feature but missing to tell it"
        self.assertEqual(check_line(line), False)


if __name__ == '__main__':
    unittest.main()
