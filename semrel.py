#!/usr/bin/env python3
"""
Check if at least one commit message will generate a release.
"""

import re
import subprocess
import sys

KEYWORDS = ["BREAKING CHANGE",
            "BREAKING CHANGES",
            "feat",
            "ci",
            "chores",
            "docs",
            "fix",
            "test",
            ]


def get_default_branch():
    """ get the default branch """
    result = subprocess.run(["git", "--no-pager", "branch"],
                            check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
    if 'main' in result:
        return 'main'
    else:
        return 'master'


def get_current_branch():
    """ get the current branch """
    result = subprocess.run(["git", "--no-pager", "branch", "--show-current"],
                            check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
    return result


def check_line(line):
    # Quick find
    if any(line.startswith(s) for s in KEYWORDS):
        print(f"Got a quick match with commit message {line}")
        for keyword in KEYWORDS:
            # Validate with a regexp
            pattern = re.compile(fr"^{keyword}(\(\w+\))?:")
            if pattern.match(line):
                print(f"Got a match with commit message {line}")
                return True
    return False


def main():
    default_branch = get_default_branch()
    current_branch = get_current_branch()
    if default_branch == current_branch:
        compare_to = f"origin/{default_branch}"
    else:
        compare_to = default_branch

    try:
        result = subprocess.run(["git", "--no-pager", "log", f"{compare_to}..",
                                 "--pretty=format:'%s'"],
                                check=True,
                                stdout=subprocess.PIPE).stdout.decode('utf-8')
    except subprocess.CalledProcessError:
        print("Error with git log")
        return 1
    for line in result.split("\n"):
        line = line.lstrip("'").rstrip("'")
        print(line)
        if check_line(line):
            return 0

    print("None of the commit messages would generate a semantic release")
    return 1


if __name__ == '__main__':
    sys.exit(main())
