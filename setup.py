from setuptools import find_packages
from setuptools import setup

setup(
    name='pre-commit-semantic-release',
    description='Pre-commit hooks for semantic release',
    url='https://gitlab.com/jrottenberg/pre-commit-semantic-release',

    author='Contributors',

    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    packages=find_packages(exclude=('tests*', 'testing*')),
    install_requires=[
    ],
    py_modules=['semrel'],
    entry_points={
        'console_scripts': [
            'semrel_check = semrel:main',
        ],

    },
)
